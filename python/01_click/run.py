import click

@click.command()
@click.option('--count', default=1, help='Number of greetings')
@click.option('--name', prompt='Your name', help='The person to greet')
def hello(count, name):
    """Simple program to greet person"""
    for x in range(count):
        click.echo('Hello %s!' % name)

@click.group()
def cli():
    pass

@cli.command()
def initdb():
    click.echo('Initializing...')

@cli.command()
def dropdb():
    click.echo('Dropping...')

if __name__ == '__main__':
    hello()

