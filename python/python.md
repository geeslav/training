# Python

- [PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)
- [Python - Tutorial](http://www.tutorialspoint.com/python/index.htm)


## Problems with PIP

### *.whl is not a supported wheel on this platform.

- Upgrade pip `python -m pip install --upgrade pip`