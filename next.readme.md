# What to train next

    Redis
    Apache CXF
    Enterprise patterns
        Enterprise Integration Patterns - Messaging Patterns Overview - https://www.enterpriseintegrationpatterns.com/patterns/messaging/
    Apache Camel
        java - What exactly is Apache Camel? - Stack Overflow - https://stackoverflow.com/questions/8845186/what-exactly-is-apache-camel
    JAX-WS
        Introduction to JAX-WS | Baeldung - https://www.baeldung.com/jax-ws
    https://snyk.io/jvm-ecosystem-report-2021/
        IDE: InrelliJ Idea
        Kotlin
        Spring Boot
    intellij gradle project
    mocha testy
    Terraform
    awk
        The UNIX School: awk - 10 examples to group data in a CSV or text file - https://www.theunixschool.com/2012/06/awk-10-examples-to-group-data-in-csv-or.html
    self signed certificates hands-on
