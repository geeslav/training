# Spring Boot Training 
using Spring Boot CLI

Spring Boot Tutorial - Bootstrap a Simple App | Baeldung - https://www.baeldung.com/spring-boot-start
GitHub - spring-projects/spring-boot: Spring Boot - https://github.com/spring-projects/spring-boot
Getting Started - https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/getting-started.html#getting-started.installing

## Training
what to do next

The spring.io web site contains many “Getting Started” guides that use Spring Boot. If you need to solve a specific problem, check there first.
Spring | Guides - https://spring.io/guides

## Development

Before we begin, open a terminal and run the following commands to ensure that you have valid versions of Java and Maven installed:

> $ java -version
java version "1.8.0_102"

> $ mvn -v
Apache Maven 3.5.4 (1edded0938998edf8bf061f1ceb3cfdeccf443fe; 2018-06-17T14:33:14-04:00)

Spring code completion
> $ . ~/.sdkman/candidates/springboot/current/shell-completion/bash/spring
> $ spring <HIT TAB HERE>
