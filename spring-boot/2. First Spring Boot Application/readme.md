# First Spring Boot Application

Getting Started - https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/getting-started.html#getting-started.first-application

with pom.xml run
> $ mvn package

Since you used the spring-boot-starter-parent POM, you have a useful run goal that you can use to start the application. Type 
> $ mvn spring-boot:run 

app listens at 
> localhost:8080

If you want to peek inside, you can use jar tvf, as follows:
> $ jar tvf target/myproject-0.0.1-SNAPSHOT.jar

To run that application, use the java -jar command, as follows:
> $ java -jar target/myproject-0.0.1-SNAPSHOT.jar
