
## Installation

### Java

> $ sdk install java 17.0.1-oracle

We can control the switching between versions in two forms, temporarily:
> $ sdk use java 14.0.1.j9-adpt

or permanently:
> $ sdk default java 14.0.1.j9-adpt

To check the current version of Java, we run the current command:
> $ sdk current java

Guide to SDKMAN! | Baeldung - https://www.baeldung.com/java-sdkman-intro

<!-- 
Using java 11

> $ export JAVA_HOME=/home/sgaspierik/opt/java/openjdk11/openjdk11

> $ ~/opt/java/openjdk11/openjdk11/bin/java -version
openjdk version "11.0.10" 2021-01-19
OpenJDK Runtime Environment AdoptOpenJDK (build 11.0.10+9)
OpenJDK 64-Bit Server VM AdoptOpenJDK (build 11.0.10+9, mixed mode)
 -->

### Maven

> $ sdk install maven

> $ mvn --version
Maven home: /home/sgaspierik/.sdkman/candidates/maven/current

> $ export PATH=/home/sgaspierik/.sdkman/candidates/maven/current/bin:$PATH

### Gradle

> $ sdk install gradle

### Spring Boot CLI
command line tool that you can use to quickly prototype with Spring

> $ sdk install springboot
Downloading: springboot 2.5.6

> $ spring --version
Spring CLI v2.5.6

code completion
> $ . ~/.sdkman/candidates/springboot/current/shell-completion/bash/spring
> $ spring <HIT TAB HERE>
