#!/bin/bash

# in_file="in.data"
# groups_file="group-name.mapping.data"
# out_file="out.data"

    # trim: sed --expression 's/^\s*//'
    #
    # s/ : Substitute command ~ replacement for pattern (^[ \t]*) on each addressed line
    # ^[ \t]* : Search pattern ( ^ – start of the line; [ \t]* match one or more blank spaces including tab)
    # The character class \s will match the whitespace characters <tab> and <space>
    # // : Replace (delete) all matched pattern

# prepare working copy
cat in.data > in.temp.data

# replace names with groups
while read groups_line; do

  group="$(echo "$groups_line" | tr -s " " | sed --expression 's/^\s*//' | cut -d ' ' -f1)"
  name="$(echo "$groups_line" | tr -s " " | sed --expression 's/^\s*//' | cut -d ' ' -f2-)"
  sed -i "s/$name/$group/" in.temp.data

done < group-name.mapping.data

# normalize file
cat in.temp.data | tr -s " " | sed --expression 's/^\s*//' | sed -r 's/\s+/,/' > norm.temp.data
# sed -r 's/\s+/,/' "$temp_file"

# group by
awk -F, '{a[$2]+=$1;}END{for(i in a)print a[i]" "i;}' norm.temp.data > out.data

# cleanup and print
\rm -f *.temp.data
cat out.data | column -t
