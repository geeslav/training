import * as fs from 'fs';

async function logChunks( readable ) {
	for await ( const chunk of readable ) {
		console.log( 'chunk: ' + chunk );
	}
}

const readable = fs.createReadStream( 'file.txt', { encoding: 'utf8'});

logChunks( readable );
