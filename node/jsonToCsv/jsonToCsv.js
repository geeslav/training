const csvjson = require('csvjson');
const readFile = require('fs').readFile;
const writeFile = require('fs').writeFile;
const fileName = 'contact.intermediary-three'
// const fileName = 'contact.intermediary'
// const fileName = 'test-data'

readFile('./' + fileName + '.json', 'utf-8', (err, fileContent) => {
    if (err) {
        console.log(err); // Do something to handle the error or just throw it
        throw new Error(err);
    }

    const csvData = csvjson.toCSV(fileContent, {
        headers: 'key'
    });
    writeFile('./' + fileName + '.csv', csvData, (err) => {
        if(err) {
            console.log(err); // Do something to handle the error or just throw it
            throw new Error(err);
        }
        console.log('Success!');
    });
});
