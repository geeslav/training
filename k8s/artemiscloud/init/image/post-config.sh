#!/bin/bash
# Then, the Operator executes the post-configuration (post-config.sh) script
# provided by the custom Init Container.

echo "#### The config dir locates at ${CONFIG_INSTANCE_DIR} ####"
ls ${CONFIG_INSTANCE_DIR}

echo "#### Copying postgresql jdbc driver jar to ${CONFIG_INSTANCE_DIR}/lib ####"
cp /amq/postgresql-jdbc-driver/postgresql-42.3.1.jar ${CONFIG_INSTANCE_DIR}/lib

echo "#### postgresql user name ${DB_USER} password ${DB_PASSWORD}"
echo "#### postgresql service ip is ${DB_SERVICE_IP} and port is ${DB_SERVICE_PORT} ####"
echo "#### database name: ${DB_NAME}"
echo "#### Adding jdbc configuration to broker.xml ####"

url="$(cat /amq/extra/configmaps/amq/url)"; echo "url: $url"
host="$(cat /amq/extra/configmaps/amq/host)"; echo "host: $host"
port="$(cat /amq/extra/configmaps/amq/port)"; echo "port: $port"
user="$(cat /amq/extra/configmaps/amq/user)"; echo "user: $user"
passwordSecret="$(cat /amq/extra/configmaps/amq/passwordSecret)"; echo "passwordSecret: $passwordSecret"

password="$(cat /amq/extra/secrets/postgresql-user-passwords-amq-amq/password)"; echo "password: $password"

jdbcStore=""
jdbcStore="${jdbcStore}<store>\n"
jdbcStore="${jdbcStore}   <database-store>\n"
jdbcStore="${jdbcStore}      <jdbc-driver-class-name>org.postgresql.Driver</jdbc-driver-class-name>\n"
jdbcStore="${jdbcStore}      <jdbc-connection-url>jdbc:postgresql://${host}:${port}/${user}?user=${user}&amp;password=${password}</jdbc-connection-url>\n"

jdbcStore="${jdbcStore}      <message-table-name>MESSAGES</message-table-name>\n"
jdbcStore="${jdbcStore}      <bindings-table-name>BINDINGS</bindings-table-name>\n"
jdbcStore="${jdbcStore}      <large-message-table-name>LARGE_MESSAGES</large-message-table-name>\n"
jdbcStore="${jdbcStore}      <page-store-table-name>PAGE_STORE</page-store-table-name>\n"
jdbcStore="${jdbcStore}      <node-manager-store-table-name>NODE_MANAGER_STORE</node-manager-store-table-name>\n"

jdbcStore="${jdbcStore}      <jdbc-lock-expiration>20000</jdbc-lock-expiration>\n"
jdbcStore="${jdbcStore}      <jdbc-lock-renew-period>4000</jdbc-lock-renew-period>\n"
jdbcStore="${jdbcStore}      <jdbc-network-timeout>20000</jdbc-network-timeout>\n"
jdbcStore="${jdbcStore}   </database-store>\n"
jdbcStore="${jdbcStore}</store>\n"

sed -i "/<persistence-enabled>/i ${jdbcStore}" ${CONFIG_INSTANCE_DIR}/etc/broker.xml


echo "#### Adding metrics plugin configuration to broker.xml ####"

cp /amq/metrics/artemis-prometheus-metrics-plugin-1.1.0.redhat-00002.jar ${CONFIG_INSTANCE_DIR}/lib
metrics_config='<metrics-plugin class-name="org.apache.activemq.artemis.core.server.metrics.plugins.ArtemisPrometheusMetricsPlugin"/>'
sed -i "/<\/configuration>/i ${metrics_config}" ${CONFIG_INSTANCE_DIR}/etc/broker.xml

echo "--- broker.xml ---"
cat ${CONFIG_INSTANCE_DIR}/etc/broker.xml


echo "#### Adding metrics plugin servlet configuration to broker.xml ####"

mkdir -p ${CONFIG_INSTANCE_DIR}/web
cp /amq/metrics/metrics.war ${CONFIG_INSTANCE_DIR}/web
metrics_servlet_config='<app url="metrics" war="metrics.war"/>'
sed -i "/<\/binding>/i ${metrics_servlet_config}" ${CONFIG_INSTANCE_DIR}/etc/bootstrap.xml

echo "--- bootstrap.xml ---"
cat ${CONFIG_INSTANCE_DIR}/etc/bootstrap.xml


echo "#### Custom config done. ####"