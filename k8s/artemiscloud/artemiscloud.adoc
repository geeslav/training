= Artemis Cloud

== 

`aac` ActiveMQ Artemis Cloud

== Install 

readme.md in operator dir

ArtemisCloud Documentation - https://artemiscloud.io/documentation/index.html

 git clone https://github.com/artemiscloud/activemq-artemis-operator.git
 kubectl create namespace aac
 kubectl config set-context $(kubectl config current-context) --namespace=aac
 kubectl create -f deploy/service_account.yaml
 kubectl create -f deploy/role.yaml
 kubectl create -f deploy/role_binding.yaml
 kubectl create -f deploy/crds/

You need to deploy all the yamls from this dir except *cluster_role.yaml* and *cluster_role_binding.yaml*:
```
kubectl create -f ./deploy/service_account.yaml
kubectl create -f ./deploy/role.yaml
kubectl create -f ./deploy/role_binding.yaml
kubectl create -f ./deploy/election_role.yaml
kubectl create -f ./deploy/election_role_binding.yaml
kubectl create -f ./deploy/operator_config.yaml
kubectl create -f ./deploy/operator.yaml
```
``` 
kubectl apply -f examples/artemis-cluster-deployment.yaml
kubectl create -f examples/address-queue-create-auto-removed.yaml
```

== Test

 kubectl exec -n aac pod/ex-aao-ss-0 -- /bin/bash /home/jboss/amq-broker/bin/artemis help producer

produce

 kubectl exec -n aac pod/ex-aao-ss-0 -- /bin/bash /home/jboss/amq-broker/bin/artemis producer --user admin --password admin --url tcp://ex-aao-ss-0:61616 --destination sfrbAddress0::sfrbQueue0 --message-count 10

 --destination <destination>
            Destination to be used. It can be prefixed with queue:// or topic://
            and can be an FQQN in the form of <address>::<queue>. (Default:
            queue://TEST)

stat

 kubectl exec -n aac pod/ex-aao-ss-0 -- /bin/bash /home/jboss/amq-broker/bin/artemis queue stat --user admin --password admin --url tcp://ex-aao-ss-0:61616

consume

 kubectl exec -n aac pod/ex-aao-ss-0 -- /bin/bash /home/jboss/amq-broker/bin/artemis queue stat --user admin --password admin --url tcp://ex-aao-ss-0:61616

scale

 kubectl apply -f examples/artemis-cluster-deployment.yaml


== Usage

 `# get aac kind resources` kubectl get all,endpoints,ingress,secret,pv,pvc,ns,configmap,Role,RoleBinding,ClusterRole,ClusterRoleBinding,crd,ActiveMQArtemis,ActiveMQArtemisAddress,ActiveMQArtemisScaledown -A|g -ve kube-sys -e kubernetes-dash -e rbac-defaults -e /system:|g -E '^N|aac'

Getting Started with the ArtemisCloud Operator - https://artemiscloud.io/blog/using_operator/

