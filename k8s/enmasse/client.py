from __future__ import print_function, unicode_literals
import optparse

# Qpid Proton Python API Documentation — Qpid Proton Python API 0.37.0 documentation
# https://qpid.apache.org/releases/qpid-proton-0.37.0/proton/python/docs/index.html
from proton import Message
from proton.handlers import MessagingHandler
from proton.reactor import Container

class HelloWorld(MessagingHandler):
    def __init__(self, url):
        super(HelloWorld, self).__init__()
        self.url = url

    # The following are some of the important event callbacks that may be
    # implemented by a developer:

    # This indicates that the event loop in the container has started, and
    # that a new sender and/or receiver may now be created.
    def on_start(self, event):
        event.container.create_receiver(self.url)
        event.container.create_sender(self.url)

    # This callback indicates that send credit has now been set by the
    # receiver, and that a message may now be sent.
    def on_sendable(self, event):
        event.sender.send(Message(body="Hello World!"))
        event.sender.close()

    # This callback indicates that a message has been received. The message
    # and its delivery object may be retreived, and if needed, the message
    # can be either accepted or rejected.
    def on_message(self, event):
        print("Received: " + event.message.body)
        event.connection.close()

parser = optparse.OptionParser(usage="usage: %prog [options]")
parser.add_option("-u", "--url", default="amqps://localhost:5672/myqueue",
                  help="url to use for sending and receiving messages")
opts, args = parser.parse_args()

try:
    Container(HelloWorld(opts.url)).run()
except KeyboardInterrupt: pass
