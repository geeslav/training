# kind load Command

Build local image, load to k8s and run

This allows a workflow like:

> docker build -t my-custom-image:unique-tag ./my-image-dir
> kind load docker-image my-custom-image:unique-tag
> kubectl apply -f my-manifest-using-my-image:unique-tag

- prepare resources

run.sh must use /bin/sh because of Alpine
run.sh must be executable

- build image

> $ docker build -t load-sample:1 .

Test run
> $ docker run load-sample:1 World

- start kind

> $ kind create cluster --name load-sample

- load image to kind

> $ kind load docker-image load-sample:1 --name load-sample

- run pod

> $ kubectl apply -f Pod.yaml
> $ kubectl logs load-sample-pod
Hello Kubernetes

- delete cluster

> $ kind delete cluster --name load-sample

