apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-ConfigMap
data:
  dessert: {{ .Values.dessert }}
  salad: {{ .Values.global.salad }}

