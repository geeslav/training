{{/*
  Documentation block
  This is named template
  Name is global
*/}}
{{- define "costr.demo.labels" }}
labels:
  generator: helm
  date: {{ now | htmlDate }}
  chartName: {{ .Chart.Name }}
{{- end }}

{{- define "costr.demo.flatLabels" }}
generator: helm
date: {{ now | htmlDate }}
chartName: {{ .Chart.Name }}
{{- end }}

