kind: ConfigMap
apiVersion: v1
metadata:
  name: {{ .Release.Name }}-ConfigMap
  {{- /* pass a scope to the template: */}}
  {{- include "costr.demo.labels" . | indent 2 }}
  labels:
    asdf: "foo"
    {{- include "costr.demo.flatLabels" . | indent 4 }}
  # debugging by comment, which are processed and printed to yaml
  # {{ .Values.global.salad | title }}
data:
  build: 5.{{ now | htmlDate }}
  {{- with .Values.favourite }}
  release: {{ $.Release.Name }}
  myvalue: "Hello World"
  drink: {{ .drink | default "tea" | quote }}
  food: {{ .food | upper | quote }}
  {{- if eq .drink "coffee" }}
  mug: "true"
  {{- end }}
  {{- end }}
  toppings: |-
    {{- range .Values.pizzaToppings }}
    - {{ . | title | quote }}
      {{- end }}
  sizes: |-
    {{- range $i, $size := tuple "small" "medium" "large" }}
    {{ $i }}: {{ $size | quote }}
    {{- end }}
  salad: {{ .Values.global.salad }}
