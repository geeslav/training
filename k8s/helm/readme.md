# Helm

Dry run with disabled validation
> $ helm install costr ./costr --debug --dry-run --disable-openapi-validation 

Helm | Template Function List - https://helm.sh/docs/chart_template_guide/function_list/
Helm | Charts - https://helm.sh/docs/topics/charts/

## Template

text within double parentheses {{}}. This is what is called a template directive.

Helm makes use of the Go template language and extends that to something called Helm template language. 

## Structure 

Inside of this directory, Helm will expect a structure that matches this:

wordpress/
  Chart.yaml          # A YAML file containing information about the chart
  values.yaml         # The default configuration values for this chart

  LICENSE             # OPTIONAL: A plain text file containing the license for the chart
  README.md           # OPTIONAL: A human-readable README file
  values.schema.json  # OPTIONAL: A JSON Schema for imposing a structure on the values.yaml file

  charts/             # A directory containing any charts upon which this chart depends.
  crds/               # Custom Resource Definitions
  templates/          # A directory of templates that, when combined with values,
                      # will generate valid Kubernetes manifest files.
  templates/NOTES.txt # OPTIONAL: A plain text file containing short usage notes

