var index = require("../index.js");
var chai = require("chai");
var expect = chai.expect;

describe("index.js test", function() {
	it("addTwoNumbers returns a number", function() {
		expect(index.addTwoNumbers(0, 0)).to.be.a("number");
	});
	it("addTwoNumbers can add 1 + 2", function() {
		expect(index.addTwoNumbers(1, 2)).to.equal(3);
	});
});

describe("Broken Async Code", function() {
	it("Async Test", function(done) {
		setTimeout(function() {
			// Intentionally failing
			expect(true).to.be.false;
			// Tells mocha to run next test
			done();
		}, 1000);
	});
});
