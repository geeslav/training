#!/usr/bin/clisp

;;; 'set' and 'setq' make the symbol point to the list

(setq
  foo '(a b c)
  bar '(d e f))

(princ foo)
(princ bar)
