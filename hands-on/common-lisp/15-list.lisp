#!/usr/bin/clisp

;; cons constructs lists; it is the inverse of car and cdr

(cons 'white '(red blue))
