#!/usr/bin/clisp

;;; eq, eql, equal, equalp and =

;; = only for numbers
;; eq compares objects (pointers)
;; eql same as eq plus number and character values
;; equal comapares whether two objects "look" the same, still case sensitive
;; equalp case insensitive variant (with numbers is format insensitive, so 42 = 42.0)
