#!/usr/bin/clisp

;;;; At the top of source files

;;; Comments at the beginning of the line

(defun test (a &optional b)
  ;; Commends indented along with code
  (do-something a)                      ; Comments indented at column 40, or the last
  (do-something-else b))                ; column + 1 space if line exceeds 38 columns



There's a neat trick to disable code which is to prefix an expression with #+(or):

(defun test (a &optional b)
  #+(or)
  (do-something a)
  (do-something-else b))

Note: #+nil usually works too, unless you happen to have a nil or :nil feature.
