#!/usr/bin/clisp

;;; Static scoped variables
;;; let, let*

(set 'staticvar 10)
(setq staticvar 12)

(defun print-static ()
  (print staticvar))

(print staticvar) ; => 12

(let ((staticvar 11))
  (print-static))

;;; Dynamically scoped variables - Special variables
;;; defvar, defparameter

(defvar *dynvar* 20)
(defvar *dynvar* 22)

(defparameter *dynparam* 30)
(defparameter *dynparam* 32)

(defun print-dynvar ()
  (print *dynvar*))

(defun print-dynparam ()
  (print *dynparam*))

(print-dynvar)    ; => 20
(print-dynparam)  ; => 32

(let ((*dynvar* 21)
      (*dynparam* 31))
  (print-dynvar)
  (print-dynparam))
