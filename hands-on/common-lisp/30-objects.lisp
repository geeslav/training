
(defclass person ()
  ((name :initarg :name :initform "Fooman" :accessor name)))

(defvar person1 (make-instance 'person :name "John"))
(defvar person2 (make-instance 'person))

(print (class-of person1))
(print (type-of person1))

(print (name person1))
; (print (inspect person1))

(print (name person2))

(error "Hello")
(print "After error")
