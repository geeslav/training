"use strict"

function test()
{
	var num = 10
	console.log("Value in function: " + num)

	{
		let num = 12
		console.log("Value in block with let: " + num)
	}

	console.log("Value in function: " + num)
}

test()
