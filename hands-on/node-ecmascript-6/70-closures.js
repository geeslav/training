/*
 * Closures - JavaScript | MDN (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
 */

"use strict"


/*
 * Lexical scoping
 */

function init()
{
	var name = 'Mozilla'

	function displayName ()
	{
		console.log(name)
	}

	displayName()
}

init()



/*
 * Closure
 */


function constructPrinter (prefix)
{
	function printer (name)
	{
		return prefix + " " + name + "!"
	}

	return printer
}

var printSalutation = constructPrinter("Hello")

console.log(printSalutation("everybody"))
