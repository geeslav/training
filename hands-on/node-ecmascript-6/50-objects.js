"use strict"

// "object constructor" syntax
let user1 = new Object()

// "object literal" syntax
let user2 = {}

// Starting with ECMAScript 5, trailing commas in object literals are legal as well:
let user3 = {
  name: "John",
  age: 30,
  sayHi() {
  	console.log("Hi from " + this.name)
  }
}

function sayHello() {
	console.log("Hello from " + this.name)
}

user3.sayHi()

user3.sayHello = sayHello
user3.sayHello()

console.log(user3)
