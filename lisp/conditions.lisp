
; Target one environment
(string= *env* "mum-uat.uksouth.cloudapp.azure.com/uat")

; Target all branches
(string-end-with "/dev" *env*)

; variable: envtype
; - local
; - standalone
; - swarm
; - docker

; Target all local environments
(eq *env-type* :local )
