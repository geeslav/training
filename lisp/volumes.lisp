
    ; Migration data only in this one env: dh10.dsdev.cloud/dev
    ; Teraz potrebujeme v podstate to iste na zakaznickych prostrediach voci servrom ktore maju v azure.
    ; Najdete to mountnute RO na UAT a MIG v /mnt/mig_data , docker volume: mig_data
    (when (or (string= *env* "dh10.dsdev.cloud/dev")
              (string= *env* "mum-mig.uksouth.cloudapp.azure.com/uat")
              (string= *env* "mum-uat.uksouth.cloudapp.azure.com/uat"))
      (def-volume
        :forservice 'java18as

        ; Name suffix: one word without underscores, creates name like mumdev_mum_<forservice>_<name>
        :name "mig"

        ; Host path: Use Host Volume. /path/on/host:/path/in/container
        ; Do not use named volume. Ignore :name
        :src "/mnt/mig_data"

        ; Container path
        :local "/mnt/mig_data"

        ; :scaled nil
        ; :external t

        ; Options for "docker volume create". Not really
        :cmd '(concat "--driver local --opt type=none --opt o=bind --opt device=/mnt/mig_data")))
