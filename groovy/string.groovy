
// Keep: digits .+-
def parseNum = { it.replaceAll( '[^\\d\\.+-]', '' ) }

// Ensure integer or fail
// Remove decimal zeroes, fail with non-zero decimal part
def parseInt = {
	def num = parseNum( it ).replaceAll( '\\.0+$', '' )
	assert ! num.contains( '.' )
	num
}

println parseNum( '$ 1,000,000.05' )

println parseInt( '$1 000 000.00' )
// println parseInt( '1,000 000.05%' )

def normalizeIntegerString( num ) {
	num.replaceAll( '[^\\d\\.+-]', '' ).replaceAll( '\\..+', '' )
}


assert parseInt( '1 000' ) == '1000'
assert parseInt( '+1 000' ) == '+1000'
assert parseInt( '-1 000' ) == '-1000'
assert parseInt( '1,000' ) == '1000'
assert parseInt( '1000.00' ) == '1000'
assert parseInt( '1,000.00' ) == '1000'
assert parseInt( '1,000.0000' ) == '1000'
assert parseInt( '1000%' ) == '1000'
assert parseInt( '$ 1000' ) == '1000'
assert parseInt( '£ 1000' ) == '1000'
assert parseInt( '£ 1000.5' ) == '1000'
// assert parseInt( '1000,00' ) == '1000'
