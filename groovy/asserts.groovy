// Groovy Language Documentation - http://docs.groovy-lang.org/docs/groovy-2.5.7/html/documentation/#_power_assertion

def a = 1
def b = 2

def calc = { x,y -> x + y }

assert a
assert a + b == 3
assert calc( a,b ) == [ a,b ].sum()
assert a + b == 4
assert a + b == 5 : 'OMG assert failed'
