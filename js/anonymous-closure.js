(function() {
	console.log( 'This is anonymous closure' )
	var myGrades = [ 95, 45, 80, 88, 66 ];

	var average = function () {
		var total = myGrades.reduce( function( accumulator, item ) {
			return accumulator + item
		}, 0)
		console.log( `Total is ${total}` )
		return total / myGrades.length
	}
	console.log( `Average is ${average()}` )

	var failing = function() {
		var myFailingGrades = myGrades.filter( function( item ) {
			return item < 70
		})
		console.log( `Failing grades count: ${myFailingGrades.length}` )
		return myFailingGrades
	}
	console.log( `My failing grades: ${failing()}` )
})()
