import static java.lang.System.out;

class Training {
	public static void main( String[] args ) {
		System.out.println( "is palindrome raw: "+ new Tests().isPalindromeRaw( "asdf" ));
		System.out.println( "is palindrome raw: "+ new Tests().isPalindromeRaw( "asdsa" ));
		// System.out.println( "is palindrome: "+ new Tests().isPalindrome( "asdf" ));
		// System.out.println( "is palindrome: "+ new Tests().isPalindrome( "asdsa" ));
		// System.out.println( "Hello world" );
	}
}
class Tests {
	boolean isPalindromeRaw( String str ) {
		for ( int i = 0; i <= str.length() / 2; i++ ) {
			out.println( "comparing "+ str.charAt( i ));
			if ( str.charAt( i ) != str.charAt( str.length() - 1 - i )) {
				return false;
			}
		}
		return true;
	}
	boolean isPalindrome( String str ) {
		StringBuilder sb = new StringBuilder( str ).reverse();
		if ( str.equals( sb.toString())) {
			return true;
		}
		return false;
	}
}
